from fastapi import FastAPI


app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.get("/mysum")
async def mysum(a: int, b:int) -> int:
    return sum([a, b])